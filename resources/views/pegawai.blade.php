@extends('layouts.admin')

@section('main-content')


    @if (session('success'))
    <div class="alert alert-success border-left-success alert-dismissible fade show" role="alert">
        {{ session('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    @if (session('status'))
        <div class="alert alert-success border-left-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800 font-weight-bold">Halaman Pegawai</h1>
        <p class="mb-4">Halaman untuk menampilkan nama pegawai PT. Solusi Intek Indonesia.</p>
    
        <!-- DataTales Example -->
        <div class="card shadow mb-2">
            <div class="card-header py-3">
                    <h4 class="m-0 font-weight-bold text-primary">Semua Pegawai</h4>
                </div>
            <div class="card-body">
                <div class="table-responsive" style="text-align: center">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Employees Name</th>
                                <th>Position</th>
                                <th>NIK</th>
                                <th>Email</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Employees Name</th>
                                <th>Position</th>
                                <th>NIK</th>
                                <th>Email</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach ($pegawai as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    <a href="{{ url('preview-user', $item->id) }}" style="color: #4A5568;text-decoration:none;">
                                        {{ $item->name }}
                                    </a>
                                </td>
                                <td>{{ $item->position }}</td>
                                <td>{{ $item->NIK }}</td>
                                <td>{{ $item->email }}</td>
                            </tr>
                            @endforeach

                            {{-- @foreach ($pegawai as $item)
                                
                            <tr>
                                <td>{{ auth::User()->user_id}}</td>
                                <td>{{ Auth::User()->name }}</td>
                                <td>{{ Auth::User()->position  }}</td>
                                <td>{{ Auth::User()->NIK  }}</td>
                                <td>{{ Auth::User()->email  }}</td>
                            </tr>
                            
                            @endforeach --}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    
    </div>


@endsection
