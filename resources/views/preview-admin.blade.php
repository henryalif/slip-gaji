@extends('layouts.admin')

@section('main-content')


    @if (session('success'))
    <div class="alert alert-success border-left-success alert-dismissible fade show" role="alert">
        {{ session('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    @if (session('status'))
        <div class="alert alert-success border-left-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Preview Slip Gaji Pegawai</h1>
        <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below.
            For more information about DataTables, please visit the <a target="_blank"
                href="https://datatables.net">official DataTables documentation</a>.</p>
    
        <!-- DataTales Example -->

        <div class="container" style="margin-left:10%; margin-top:3%">
            <div class="row">
                <div class="col-5">
                    <img src="https://images-na.ssl-images-amazon.com/images/I/112fW7ZiHvL._SX331_BO1,204,203,200_.jpg" alt="">
                </div>
                <div class="col-5" style="margin-top:10px">
                    <button type="button" class="btn btn-primary btn-lg btn-block">Download</button>
                    <button type="button" class="btn btn-danger btn-lg btn-block">Back</button>
                </div>
            </div>
        </div>
    
    </div>


@endsection
