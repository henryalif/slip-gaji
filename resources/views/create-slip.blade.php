@extends('layouts.admin')

@section('main-content')
    
<div class="card border-bottom-info shadow mb-4">
    <div class="card-header">
    </div>

    <div class="card-body">
        <form action="{{ url('save') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{-- <div class="form-group">
                <label for="exampleFormControlInput1">Nama Pegawai</label>
                <input name="nama_pegawai" id="nama_pegawai" type="text" class="form-control" placeholder="Isi Nama Pegawai" required>
            </div> --}}

            <div class="form-group">
                <label for="exampleFormControlSelect1">Nama Pegawai</label>
                <select class="form-control" id="exampleFormControlSelect1" name="user_id">
                  <option selected>-- Pilih --</option>

                @foreach (App\User::get() as $user)
                  <option value='{{ $user->id }}'>
                    {{ $user->name }}
                </option>
                @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="exampleFormControlInput1">Gaji Pokok</label>
                <input name="gaji_pokok" id="gaji_pokok" type="text" class="form-control" placeholder="Isi Gaji Pokok" required>
            </div>

            <div class="form-group">
                <label for="exampleFormControlInput1">Tunjangan Jabatan</label>
                <input name="tunjangan_jabatan" id="tunjangan_jabatan" type="text" class="form-control" placeholder="Isi Tunjangan Jabatan" required>
            </div>

            <div class="form-group">
                <label for="exampleFormControlInput1">Tunjangan Kinerja</label>
                <input name="tunjangan_project" id="tunjangan_project" type="text" class="form-control" placeholder="Isi Tunjangan Kinerja" required>
            </div>

            <div class="form-group">
                <label for="exampleFormControlInput1">Kehadiran</label>
                <input name="kehadiran" id="kehadiran" type="text" class="form-control" placeholder="Isi Kehadiran" required>
            </div>

            <div class="form-group">
                <label for="exampleFormControlInput1">Lembur</label>
                <input name="lembur" id="lembur" type="text" class="form-control" placeholder="Isi Lembur" required>
            </div>

            <div class="form-group">
                <label for="exampleFormControlInput1">Pinjaman Karyawan</label>
                <input name="pinjaman_karyawan" id="pinjaman_karyawan" type="text" class="form-control" placeholder="Isi Pinjaman Karyawan" required>
            </div>

            <div class="form-group">
                <label for="exampleFormControlInput1">PPH Karyawan</label>
                <input name="pph_karyawan" id="pph_karyawan" type="text" class="form-control" placeholder="Isi PPH Karyawan" required>
            </div>

            <div class="form-group">
                <label for="exampleFormControlInput1">Bulan</label>
                <select name="bulan" id="bulan" class="form-control" aria-label="Default select example">
                    <option selected>-- Pilih --</option>
                    <option value="Januari">Januari</option>
                    <option value="Februari">Februari</option>
                    <option value="Maret">Maret</option>
                    <option value="April">April</option>
                    <option value="Mei">Mei</option>
                    <option value="Juni">Juni</option>
                    <option value="Juli">Juli</option>
                    <option value="Agustus">Agustus</option>
                    <option value="September">September</option>
                    <option value="Oktober">Oktober</option>
                    <option value="November">November</option>
                    <option value="Desember">Desember</option>
                  </select>
                {{-- <input name="bulan" id="bulan" type="text" class="form-control" placeholder="Isi Bulan" required> --}}
            </div>


            <div class="form-group">
                <button type="submit" class="btn btn-success float-right">Tambahkan</button>
                <a href="{{ route('slip-pegawai') }}" class="btn btn-secondary float-right" style="margin-right: 1rem">Kembali</a>
            </div>
        </form>
    </div>

</div>

@endsection