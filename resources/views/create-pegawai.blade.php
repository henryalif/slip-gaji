@extends('layouts.admin')

@section('main-content')
    
<div class="card border-bottom-info shadow mb-4">
    <div class="card-header">
    </div>

    <div class="card-body">
        <form action="{{ url('save') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="exampleFormControlInput1">Nama Pegawai</label>
                <input name="nama_pegawai" id="nama_pegawai" type="text" class="form-control" placeholder="Isi Nama Pegawai" required>
            </div>

            <div class="form-group">
                <label for="exampleFormControlInput1">Jabatan</label>
                <input name="jabatan" id="jabatan" type="text" class="form-control" placeholder="Isi Jabatan" required>
            </div>

            
            <div class="form-group">
                <label for="exampleFormControlInput1">NIK</label>
                <input name="nik" id="nik" type="text" class="form-control" placeholder="Isi NIK Maximal 16 Digit" required>
            </div>
            
            <div class="form-group">
                <label for="exampleFormControlInput1">Email</label>
                <input name="email" id="email" type="email" class="form-control" placeholder="Isi Email" required>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success float-right">Tambahkan</button>
                <a href="{{ route('pegawai') }}" class="btn btn-secondary float-right" style="margin-right: 1rem">Kembali</a>
            </div>
        </form>
    </div>

</div>

@endsection