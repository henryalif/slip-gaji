@extends('layouts.admin')

@section('main-content')


    @if (session('success'))
    <div class="alert alert-success border-left-success alert-dismissible fade show" role="alert">
        {{ session('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    @if (session('status'))
        <div class="alert alert-success border-left-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800 font-weight-bold">Preview Slip Gaji Pegawai</h1>
        <p class="mb-4">Halaman preview Slip Gaji pegawai PT. Solusi Intek Indonesia.</p>
    
        <!-- DataTales Example -->

        <div class="container-fluid" style="margin-left:10%; margin-top:3%">
            <div class="row">
                <div class="col-4">
                    <div class="card" style="width: 18rem;">
                        <img src="https://images-na.ssl-images-amazon.com/images/I/112fW7ZiHvL._SX331_BO1,204,203,200_.jpg" alt="" class="card-img-top">
                      </div>
                    
                </div>
                <div class="col-5">
                    @foreach ($pegawai as $item)
                    @endforeach
                    
                    <h5>Nama Lengkap</h5>
                    <h4><strong> {{}} </strong> </h4>
                    <br>
                    <h5>Position</h5>
                    <h4><strong>  </strong> </h4>
                    <br>
                    <h5>Salary Month</h5>
                    <h4>April - <strong>Rp. 9.000.000</strong></h4>
                    <br>
                    <button type="button" class="btn btn-primary btn-lg btn-block">Download</button>
                    
                    {{-- <h5>Nama Lengkap</h5>
                    <h4>{{ Auth::user()->name }}</h4>
                    <br>
                    <h5>Position</h5>
                    <h4>{{  Auth::user()->last_name }}</h4>
                    <br>
                    <h5>Salary Month</h5>
                    <h4>April - <strong>Rp. 9.000.000</strong></h4>
                    <br>
                    <button type="button" class="btn btn-primary btn-lg btn-block">Download</button> --}}

                </div>
            </div>
        </div>
    
    </div>


@endsection
