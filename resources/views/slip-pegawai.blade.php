@extends('layouts.admin')

@section('main-content')


    @if (session('success'))
    <div class="alert alert-success border-left-success alert-dismissible fade show" role="alert">
        {{ session('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    @if (session('status'))
        <div class="alert alert-success border-left-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800 font-weight-bold"> Halaman Slip Gaji Pegawai</h1>
        <p class="mb-4">Halaman untuk menampilkan data Slip Gaji pegawai PT. Solusi Intek Indonesia.</p>
    
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h4 class="m-0 font-weight-bold text-primary">Semua Slip Gaji Pegawai
                    <a class="btn btn-primary" href="create-slip" style="float: right" role="button"><i class="fas fa-plus-circle"></i></a>
                </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive" style="text-align: center">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Gaji Pokok</th>
                                <th>Tj. Jabatan</th>
                                <th>Tj. Kinerja</th>
                                <th>Tj. Project</th>
                                <th>Kehadiran</th>
                                <th>Lembur</th>
                                <th>Pj. Karyawan</th>
                                <th>PPH</th>
                                <th>Bulan</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Gaji Pokok</th>
                                <th>Tj. Jabatan</th>
                                <th>Tj. Kinerja</th>
                                <th>Tj. Project</th>
                                <th>Kehadiran</th>
                                <th>Lembur</th>
                                <th>Pj. Karyawan</th>
                                <th>PPH</th>
                                <th>Bulan</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach ($test as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                {{-- <td>{{ $item->relasiUser->name_id }}</td> --}}
                                {{-- <td>{{ optional($item->relasiUser)->name }}</td> --}}
                                {{-- <td>{{ $item->user->name }}</td> --}}
                                <td>

                                    <a href="{{ url('preview-user', $item->id) }}" style="color: #4A5568; text-decoration:none;">
                                        {{ $item->user->name }}
                                    </a>
                                </td>
                                <td>{{ number_format($item->gaji_pokok) }}</td>
                                <td>{{ number_format($item->tunjangan_jabatan) }}</td>
                                <td>{{ number_format($item->tunjangan_kinerja) }}</td>
                                <td>{{ number_format($item->tunjangan_project) }}</td>
                                <td>{{ $item->kehadiran }}</td>
                                <td>{{ $item->lembur }}</td>
                                <td>{{ number_format($item->pinjaman_karyawan) }}</td>
                                <td>{{ number_format($item->pph_karyawan) }}</td>
                                <td>{{ $item->bulan }}</td>
                                <td>
                                    <a href="{{ url('edit-slip', $item->id) }}"><i class="fas fa-edit"></i></a>
                                                    
                                    <a href="{{ url('destroy-slip', $item->id) }}"><i class="fas fa-trash-alt" style="color: red"></i></a>
                                </td>
                                {{-- <td>
                                    <a href="{{ url('preview-user', $item->user_id ) }}"><i class="fas fa-external-link-alt"></i></a>
                                </td> --}}
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    
    </div>


@endsection
