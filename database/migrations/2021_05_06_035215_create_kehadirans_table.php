<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKehadiransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kehadiran', function (Blueprint $table) {
            $table->id('id');
            $table->bigInteger('masuk')->nullable();
            $table->bigInteger('absen')->nullable();
            $table->bigInteger('telat_konfirmasi')->nullable();
            $table->bigInteger('telat_nonkonfirmasi')->nullable();
            $table->bigInteger('sakit_skd')->nullable();
            $table->bigInteger('sakit_nonskd')->nullable();
            $table->bigInteger('izin')->nullable();
            $table->bigInteger('ot_hour')->nullable();
            $table->bigInteger('cuti')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kehadiran');
    }
}
