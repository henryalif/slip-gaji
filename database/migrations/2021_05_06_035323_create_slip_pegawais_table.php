<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlipPegawaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slip_pegawai', function (Blueprint $table) {
            $table->id('id');
            $table->foreignId('user_id')->constrained();
            $table->bigInteger('gaji_pokok')->nullable();
            $table->bigInteger('tunjangan_jabatan')->nullable();
            $table->bigInteger('tunjangan_kinerja')->nullable();
            $table->bigInteger('tunjangan_project')->nullable();
            $table->bigInteger('kehadiran')->nullable();
            $table->bigInteger('lembur')->nullable();
            $table->bigInteger('pinjaman_karyawan')->nullable();
            $table->bigInteger('pph_karyawan')->nullable();
            $table->string('bulan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slip_pegawai');
    }
}
