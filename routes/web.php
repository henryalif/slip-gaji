<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PegawaiController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SlipController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('auth.login'); });
Route::get('pegawai', function () { return view('pegawai'); });
Route::get('create-slip', function () { return view('create-slip'); });
Route::get('edit-slip', function () { return view('edit-slip'); });

Route::get('/slip-pegawai', function () { return view('slip-pegawai'); })->name('slip-pegawai');
Route::get('/preview-user', function () { return view('preview-user'); })->name('preview-user');
Route::get('/preview-admin', function () { return view('preview-admin'); })->name('preview-admin');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/pegawai', 'UserController@index')->name('pegawai');
// Route::get('/edit-pegawai/{id}', 'UserController@edit')->name('edit-pegawai');
// Route::get('/update-pegawai/{id}', 'UserController@update')->name('update-pegawai');

Route::get('/preview-user', 'SlipController@testSlip')->name('preview-user');

// Route::get('/profile', 'ProfileController@index')->name('profile');
// Route::put('/profile', 'ProfileController@update')->name('profile.update');

// Route::get('/about', function () { return view('about'); })->name('about');

Route::get('/slip-pegawai', [SlipController::class, 'index'])->name('slip-pegawai');

// Route::get('pegawai', [PegawaiController::class, 'index']);
Route::get('create-slip', [SlipController::class, 'create'])->name('create-slip');
Route::post('save', [SlipController::class, 'store'])->name('save');
Route::get('edit-slip/{id}', [SlipController::class, 'edit'])->name('edit-slip');
Route::post('update-slip/{id}', [SlipController::class, 'update'])->name('update-slip');
Route::get('destroy-slip/{id}', [SlipController::class, 'destroy'])->name('destroy-slip');

Route::get('preview-user/{id}', [UserController::class, 'goto'])->name('preview-user');
