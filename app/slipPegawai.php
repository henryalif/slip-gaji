<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class slipPegawai extends Model
{
    use HasFactory;

    protected $table = "slip_pegawai";
    protected $fillable = [
    'user_id',
    'gaji_pokok',
    'tunjangan_jabatan',
    'tunjangan_kinerja',
    'tunjangan_project',
    'kehadiran',
    'lembur',
    'pinjaman_karyawan',
    'pph_karyawan',
    'bulan',

    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
}
