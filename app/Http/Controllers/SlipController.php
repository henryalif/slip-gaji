<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\slipPegawai;

class SlipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $test = slipPegawai::orderBy('user_id', 'asc')->get();
        return view('slip-pegawai', compact('test'));
    }

    public function testSlip()
    {   
        $pegawai = slipPegawai::all();
        return view('preview-user', compact('pegawai'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create-slip');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        slipPegawai::create([
            'user_id'           => $request->user_id,
            'gaji_pokok'        => $request->gaji_pokok,
            'tunjangan_jabatan' => $request->tunjangan_jabatan,
            'tunjangan_kinerja' => $request->tunjangan_kinerja,
            'tunjangan_project' => $request->tunjangan_project,
            'kehadiran'         => $request->kehadiran,
            'lembur'            => $request->lembur,
            'pinjaman_karyawan' => $request->pinjaman_karyawan,
            'pph_karyawan'      => $request->pph_karyawan,
            'bulan'             => $request->bulan,

            
            ]);
        
            return redirect('slip-pegawai');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)   
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slippegawai_id)
    {
        $editPg = slipPegawai::findorfail($slippegawai_id);
        return view('edit-slip', compact('editPg'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slippegawai_id)
    {
        $editPg = slipPegawai::findorfail($slippegawai_id);
        $editPg->update($request->all());
    
        return redirect('slip-pegawai');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slippegawai_id)
    {
        $editPg = slipPegawai::findorfail($slippegawai_id);
        $editPg->delete();
        return back();
    }
}
