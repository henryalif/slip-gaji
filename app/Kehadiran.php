<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kehadiran extends Model
{
    use HasFactory;

    protected $table = "kehadiran";
    protected $primarykey = "kehadiran_id";
    protected $fillable = [
    
    'masuk',
    'absen',
    'telat_konfirmasi',
    'telat_nonkonfirmasi',
    'sakit_skd',
    'sakit_nonskd',
    'izin',
    'ot_hour',
    'cuti',

    ];


}
